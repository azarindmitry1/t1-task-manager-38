# TASK-MANAGER

## DEVELOPER

**NAME:** Dmitry Azarin

**E-MAIL:** azarindmitry1@gmail.com

**SKYPE:** azarindmitry1

## SOFTWARE

**Java:** JDK 1.8

**OS:** Windows 10

## HARDWARE

**CPU:** i5

**RAM:** 16 GB

**SSD:** 256 GB

## APPLICATION RUN

```bash
java -jar ./task-manager.jar
```

## APPLICATION BUILD

```
mvn clean install
```