package ru.t1.azarin.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.component.Bootstrap;

import java.sql.SQLException;

public final class Application {

    public static void main(@Nullable String[] args) throws SQLException {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
