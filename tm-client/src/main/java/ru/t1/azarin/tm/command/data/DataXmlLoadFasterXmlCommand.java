package ru.t1.azarin.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.request.data.DataXmlLoadFasterXmlRequest;

public final class DataXmlLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public final static String NAME = "data-load-xml";

    @NotNull
    public final static String DESCRIPTION = "Load data from xml file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE XML]");
        @NotNull final DataXmlLoadFasterXmlRequest request = new DataXmlLoadFasterXmlRequest(getToken());
        getDomainEndpoint().xmlLoadFasterXmlResponse(request);
    }

    @Override
    public @Nullable String getName() {
        return NAME;
    }

    @Override
    public @Nullable String getDescription() {
        return DESCRIPTION;
    }

}
