package ru.t1.azarin.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.request.data.DataYamlLoadFasterXmlRequest;

public final class DataYamlLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public final static String NAME = "data-load-yaml";

    @NotNull
    public final static String DESCRIPTION = "Load data from yaml file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD YAML]");
        @NotNull final DataYamlLoadFasterXmlRequest request = new DataYamlLoadFasterXmlRequest(getToken());
        getDomainEndpoint().yamlLoadFasterXmlResponse(request);
    }

    @Override
    public @Nullable String getName() {
        return NAME;
    }

    @Override
    public @Nullable String getDescription() {
        return DESCRIPTION;
    }

}
