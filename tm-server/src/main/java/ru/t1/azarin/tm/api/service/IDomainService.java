package ru.t1.azarin.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.Domain;

public interface IDomainService {

    Domain getDomain();

    void setDomain(@Nullable Domain domain);

    @SneakyThrows
    void backupLoad();

    @SneakyThrows
    void backupSave();

    @SneakyThrows
    void base64Load();

    @SneakyThrows
    void base64Save();

    @SneakyThrows
    void binaryLoad();

    @SneakyThrows
    void binarySave();

    @SneakyThrows
    void jsonLoadFasterXml();

    @SneakyThrows
    void jsonLoadJaxb();

    @SneakyThrows
    void jsonSaveFasterXml();

    @SneakyThrows
    void jsonSaveJaxb();

    @SneakyThrows
    void xmlLoadFasterXml();

    @SneakyThrows
    void xmlLoadJaxb();

    @SneakyThrows
    void xmlSaveFasterXml();

    @SneakyThrows
    void xmlSaveJaxb();

    @SneakyThrows
    void yamlLoadFasterXml();

    @SneakyThrows
    void yamlSaveFasterXml();

}
