package ru.t1.azarin.tm.api.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    @SneakyThrows
    Task update(@NotNull Task task);

    @NotNull
    @SneakyThrows
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}