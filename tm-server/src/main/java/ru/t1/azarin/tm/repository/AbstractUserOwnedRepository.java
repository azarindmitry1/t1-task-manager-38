package ru.t1.azarin.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.api.repository.IUserOwnedRepository;
import ru.t1.azarin.tm.enumerated.Sort;
import ru.t1.azarin.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M>
        implements IUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    @Nullable
    public M add(@Nullable final String userId, @Nullable final M model) throws SQLException {
        if (userId == null || userId.isEmpty()) return null;
        if (model == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE user_id = ?", getTableName());
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, userId);
            preparedStatement.executeUpdate();
        }
    }

    @Override
    public boolean existById(@Nullable final String userId, @Nullable final String id) {
        return findOneById(userId, id) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE user_id = ?", getTableName());
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, userId);
            @NotNull final ResultSet rowSet = preparedStatement.executeQuery();
            while (rowSet.next()) result.add(fetch(rowSet));
        }
        return result;
    }

    @Override
    @Nullable
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        @NotNull final List<M> result = new ArrayList<>();
        if (sort == null) return findAll(userId);
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE user_id = ? ORDER_BY %s",
                getTableName(), sort.getComparator()
        );
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, userId);
            @NotNull final ResultSet rowSet = preparedStatement.executeQuery();
            while (rowSet.next()) result.add(fetch(rowSet));
        }
        return result;
    }

    @Override
    @Nullable
    @SneakyThrows
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE user_id = ? AND id = ?", getTableName());
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, userId);
            preparedStatement.setString(2, id);
            @NotNull final ResultSet rowSet = preparedStatement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @Override
    @Nullable
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) return null;
        if (model == null) return null;
        return removeById(userId, model.getId());
    }

    @Override
    @Nullable
    @SneakyThrows
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        @NotNull final String sql = String.format("DELETE FROM %s WHERE user_id = ? AND id = ?", getTableName());
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, userId);
            preparedStatement.setString(2, id);
            preparedStatement.executeUpdate();
        }
        return model;
    }

}
