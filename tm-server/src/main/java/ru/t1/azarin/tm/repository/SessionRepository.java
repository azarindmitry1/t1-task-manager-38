package ru.t1.azarin.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.api.repository.ISessionRepository;
import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.model.Session;

import java.sql.*;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    private final String tableName = "tm_session";

    @NotNull
    @Override
    protected String getTableName() {
        return tableName;
    }

    @NotNull
    @Override
    protected Session fetch(@NotNull ResultSet row) throws SQLException {
        @NotNull final Session session = new Session();
        session.setId(row.getString("id"));
        session.setUserId(row.getString("user_id"));
        session.setDate(row.getTimestamp("date"));
        session.setRole(Role.valueOf(row.getString("role")));
        return session;
    }

    @NotNull
    @Override
    public Session add(@NotNull final Session session) throws SQLException {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (id, user_id, date, role) VALUES (?, ?, ?, ?)", getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, session.getId());
            statement.setString(2, session.getUserId());
            statement.setTimestamp(3, new Timestamp(session.getDate().getTime()));
            statement.setString(4, session.getRole().toString());
            statement.executeUpdate();
        }
        return session;
    }

    @NotNull
    @Override
    public Session add(@NotNull final String userId, @NotNull final Session session) throws SQLException {
        session.setUserId(userId);
        return add(session);
    }

    public void update(@NotNull final Session session) throws SQLException {
        @NotNull final String sql = String.format(
                "UPDATE %s SET userId = ?, date = ?, role = ? WHERE id = ?",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, session.getUserId());
            statement.setTimestamp(2, new Timestamp(session.getDate().getTime()));
            statement.setString(3, session.getRole().toString());
            statement.setString(4, session.getId());
            statement.executeUpdate();
        }
    }

}
