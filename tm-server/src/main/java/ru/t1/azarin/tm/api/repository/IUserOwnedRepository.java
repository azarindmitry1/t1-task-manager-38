package ru.t1.azarin.tm.api.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.enumerated.Sort;
import ru.t1.azarin.tm.model.AbstractUserOwnedModel;

import java.sql.SQLException;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    M add(@Nullable String userId, M model) throws SQLException;

    @SneakyThrows
    void clear(@Nullable String userId);

    boolean existById(@Nullable String userId, @Nullable String id);

    @NotNull
    @SneakyThrows
    List<M> findAll(@Nullable String userId);

    @Nullable
    @SneakyThrows
    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

    @SneakyThrows
    M findOneById(@Nullable String userId, @Nullable String id);

    M remove(@Nullable String userId, M model);

    @SneakyThrows
    M removeById(@Nullable String userId, @Nullable String id);

}
