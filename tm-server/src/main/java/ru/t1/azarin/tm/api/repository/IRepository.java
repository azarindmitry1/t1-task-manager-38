package ru.t1.azarin.tm.api.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.model.AbstractModel;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @SneakyThrows
    void clear();

    @Nullable
    M add(@NotNull M model) throws SQLException;

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    @NotNull
    @SneakyThrows
    List<M> findAll();

    @SneakyThrows
    M findOneById(@Nullable String id);

    boolean existById(@Nullable String id);

    M remove(M model) throws SQLException;

    @SneakyThrows
    M removeById(@Nullable String id);

}
