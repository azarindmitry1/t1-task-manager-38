package ru.t1.azarin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.enumerated.Status;
import ru.t1.azarin.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    Project updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

}