package ru.t1.azarin.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.api.repository.IProjectRepository;
import ru.t1.azarin.tm.enumerated.Status;
import ru.t1.azarin.tm.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    private final String tableName = "tm_project";

    @NotNull
    @Override
    protected String getTableName() {
        return tableName;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project fetch(@NotNull final ResultSet row) {
        @NotNull final Project project = new Project();
        project.setId(row.getString("id"));
        project.setUserId(row.getString("user_id"));
        project.setName(row.getString("name"));
        project.setDescription(row.getString("description"));
        project.setStatus(Status.toStatus(row.getString("status")));
        project.setCreated(row.getTimestamp("created"));
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project add(@NotNull final Project project) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (id, user_id, name, description, status, created) " +
                        "VALUES (?, ?, ? , ?, ?, ?)", getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getId());
            statement.setString(2, project.getUserId());
            statement.setString(3, project.getName());
            statement.setString(4, project.getDescription());
            statement.setString(5, Status.NOT_STARTED.toString());
            statement.setTimestamp(6, new Timestamp(project.getCreated().getTime()));
            statement.executeUpdate();
        }
        return project;
    }

    @NotNull
    @Override
    public Project add(@NotNull final String userId, @NotNull final Project project) {
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project update(@NotNull final Project project) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET name = ?, description = ? WHERE id = ?", getTableName()
        );
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, project.getName());
            preparedStatement.setString(2, project.getDescription());
            //preparedStatement.setString(3, project.getStatus().toString());
            preparedStatement.setString(3, project.getId());
            preparedStatement.executeUpdate();
        }
        return project;
    }

}