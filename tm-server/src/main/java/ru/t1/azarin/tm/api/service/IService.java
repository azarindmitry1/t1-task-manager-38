package ru.t1.azarin.tm.api.service;

import ru.t1.azarin.tm.api.repository.IRepository;
import ru.t1.azarin.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {
}
