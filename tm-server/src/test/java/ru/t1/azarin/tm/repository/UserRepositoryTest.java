package ru.t1.azarin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.azarin.tm.api.repository.IUserRepository;
import ru.t1.azarin.tm.api.service.IConnectionService;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.marker.UnitCategory;
import ru.t1.azarin.tm.model.User;
import ru.t1.azarin.tm.service.ConnectionService;
import ru.t1.azarin.tm.service.PropertyService;
import ru.t1.azarin.tm.util.HashUtil;

import java.sql.SQLException;

import static ru.t1.azarin.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final IUserRepository USER_REPOSITORY = new UserRepository(CONNECTION_SERVICE.getConnection());

    @NotNull
    private final String testLogin = "test_login";

    @NotNull
    private final String testPassword = "test_password";

    @Before
    public void before() throws Exception {
        USER_REPOSITORY.add(USER1);
    }

    @After
    public void after() {
        USER_REPOSITORY.removeById(USER1.getId());
    }

    @Test
    public void add() throws SQLException {
        @NotNull final User user = new User();
        user.setLogin(testLogin);
        user.setPasswordHash(HashUtil.salt(PROPERTY_SERVICE, testPassword));
        USER_REPOSITORY.add(user);
        @Nullable final User createdUser = USER_REPOSITORY.findOneById(user.getId());
        Assert.assertNotNull(createdUser);
        Assert.assertEquals(createdUser.getLogin(), user.getLogin());
    }

    @Test
    public void clear() {
        Assert.assertFalse(USER_REPOSITORY.findAll().isEmpty());
        USER_REPOSITORY.clear();
        Assert.assertEquals(0, USER_REPOSITORY.findAll().size());
    }

    @Test
    public void findAll() {
        Assert.assertEquals(2, USER_REPOSITORY.findAll().size());
    }

    @Test
    public void findOneById() {
        @Nullable final User user = USER_REPOSITORY.findOneById(USER1.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER1.getId(), user.getId());
    }

    @Test
    public void findByLogin() {
        @Nullable final User user = USER_REPOSITORY.findByLogin(USER1_LOGIN);
        Assert.assertNotNull(user);
        Assert.assertEquals(USER1.getLogin(), user.getLogin());
    }

    @Test
    public void findByEmail() {
        @Nullable final User user = USER_REPOSITORY.findByEmail(USER1_EMAIL);
        Assert.assertNotNull(user);
        Assert.assertEquals(USER1_EMAIL, user.getEmail());
    }

    @Test
    public void existById() {
        Assert.assertTrue(USER_REPOSITORY.existById(USER1.getId()));
        Assert.assertFalse(USER_REPOSITORY.existById(USER_UNREGISTRY_ID));
    }

    @Test
    public void isLoginExist() {
        Assert.assertTrue(USER_REPOSITORY.isLoginExist(USER1_LOGIN));
        Assert.assertFalse(USER_REPOSITORY.isLoginExist(USER_UNREGISTRY_LOGIN));
    }

    @Test
    public void isEmailExist() {
        Assert.assertTrue(USER_REPOSITORY.isEmailExist(USER1.getEmail()));
        Assert.assertFalse(USER_REPOSITORY.isEmailExist(USER_UNREGISTRY_EMAIL));
    }

    @Test
    public void remove() throws SQLException {
        @Nullable final User user = USER_REPOSITORY.findOneById(USER1.getId());
        Assert.assertNotNull(user);
        USER_REPOSITORY.remove(USER1);
        Assert.assertNull(USER_REPOSITORY.findOneById(USER1.getId()));
    }

    @Test
    public void removeById() {
        @Nullable final User user = USER_REPOSITORY.findOneById(USER1.getId());
        Assert.assertNotNull(user);
        USER_REPOSITORY.removeById(USER1.getId());
        Assert.assertNull(USER_REPOSITORY.findOneById(USER1.getId()));
    }

}
