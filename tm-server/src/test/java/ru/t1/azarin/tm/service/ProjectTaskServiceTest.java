package ru.t1.azarin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.azarin.tm.api.repository.IUserRepository;
import ru.t1.azarin.tm.api.service.*;
import ru.t1.azarin.tm.exception.field.ProjectIdEmptyException;
import ru.t1.azarin.tm.exception.field.TaskIdEmptyException;
import ru.t1.azarin.tm.exception.field.UserIdEmptyException;
import ru.t1.azarin.tm.marker.UnitCategory;
import ru.t1.azarin.tm.model.Task;
import ru.t1.azarin.tm.model.User;
import ru.t1.azarin.tm.repository.UserRepository;
import ru.t1.azarin.tm.util.HashUtil;

import java.sql.SQLException;

import static ru.t1.azarin.tm.constant.ProjectTestData.USER1_PROJECT1;
import static ru.t1.azarin.tm.constant.ProjectTestData.USER1_PROJECT2;
import static ru.t1.azarin.tm.constant.TaskTestData.USER1_TASK1;
import static ru.t1.azarin.tm.constant.TaskTestData.USER1_TASK2;
import static ru.t1.azarin.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class ProjectTaskServiceTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private final IProjectService projectService = new ProjectService(CONNECTION_SERVICE);

    @NotNull
    private final ITaskService taskService = new TaskService(CONNECTION_SERVICE);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(CONNECTION_SERVICE);

    @NotNull
    private static final IUserRepository USER_REPOSITORY = new UserRepository(CONNECTION_SERVICE.getConnection());

    @NotNull
    private static String USER_ID = "";

    @NotNull
    private final String emptyString = "";

    @Nullable
    private final String nullString = null;

    @BeforeClass
    public static void setUp() throws SQLException {
        @NotNull final User user = new User();
        user.setLogin(USER1_LOGIN);
        user.setPasswordHash(HashUtil.salt(PROPERTY_SERVICE, USER1_PASSWORD));
        user.setEmail(USER1_EMAIL);
        USER_REPOSITORY.add(user);
        USER_ID = user.getId();
    }

    @AfterClass
    public static void tearDown() throws SQLException {
        @Nullable final User user = USER_REPOSITORY.findByLogin(USER1_LOGIN);
        USER_REPOSITORY.remove(user);
    }

    @Before
    public void before() throws SQLException {
        projectService.add(USER_ID, USER1_PROJECT1);
        projectService.add(USER_ID, USER1_PROJECT2);
        taskService.add(USER_ID, USER1_TASK1);
        taskService.add(USER_ID, USER1_TASK2);
    }

    @After
    public void after() {
        projectService.clear(USER_ID);
        taskService.clear(USER_ID);
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.bindTaskToProject(
                emptyString, USER1_PROJECT1.getId(), USER1_TASK1.getId())
        );
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.bindTaskToProject(
                nullString, USER1_PROJECT1.getId(), USER1_TASK1.getId())
        );
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject(
                USER1.getId(), emptyString, USER1_TASK1.getId())
        );
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject(
                USER1.getId(), nullString, USER1_TASK1.getId())
        );
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject(
                USER1.getId(), USER1_PROJECT1.getId(), emptyString)
        );
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject(
                USER1.getId(), USER1_PROJECT1.getId(), nullString)
        );
        projectTaskService.bindTaskToProject(USER_ID, USER1_PROJECT1.getId(), USER1_TASK1.getId());
        @Nullable final Task task = taskService.findOneById(USER_ID, USER1_TASK1.getId());
        Assert.assertEquals(USER1_PROJECT1.getId(), task.getProjectId());
    }

    @Test
    public void unbindTaskToProject() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.unbindTaskToProject(
                emptyString, USER1_PROJECT1.getId(), USER1_TASK1.getId())
        );
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.unbindTaskToProject(
                nullString, USER1_PROJECT1.getId(), USER1_TASK1.getId())
        );
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.unbindTaskToProject(
                USER_ID, emptyString, USER1_TASK1.getId())
        );
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.unbindTaskToProject(
                USER_ID, nullString, USER1_TASK1.getId())
        );
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.unbindTaskToProject(
                USER_ID, USER1_PROJECT1.getId(), emptyString)
        );
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.unbindTaskToProject(
                USER_ID, USER1_PROJECT1.getId(), nullString)
        );
        projectTaskService.unbindTaskToProject(USER_ID, USER1_PROJECT1.getId(), USER1_TASK1.getId());
        Assert.assertNull(USER1_TASK1.getProjectId());
    }

    @Test
    public void removeProjectById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.removeProjectById(
                emptyString, USER1_PROJECT1.getId())
        );
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.removeProjectById(
                nullString, USER1_PROJECT1.getId())
        );
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById(
                USER_ID, emptyString)
        );
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById(
                USER_ID, nullString)
        );
        projectTaskService.removeProjectById(USER_ID, USER1_PROJECT1.getId());
        Assert.assertNull(projectService.findOneById(USER1_PROJECT1.getId()));
    }

}
