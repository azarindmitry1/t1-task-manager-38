package ru.t1.azarin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.azarin.tm.api.repository.ISessionRepository;
import ru.t1.azarin.tm.api.repository.IUserRepository;
import ru.t1.azarin.tm.api.service.IConnectionService;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.marker.UnitCategory;
import ru.t1.azarin.tm.model.Session;
import ru.t1.azarin.tm.model.User;
import ru.t1.azarin.tm.service.ConnectionService;
import ru.t1.azarin.tm.service.PropertyService;
import ru.t1.azarin.tm.util.HashUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;

import static ru.t1.azarin.tm.constant.SessionTestData.USER1_SESSION1;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final Connection CONNECTION = CONNECTION_SERVICE.getConnection();

    @NotNull
    private static final ISessionRepository SESSION_REPOSITORY = new SessionRepository(CONNECTION);

    @NotNull
    private static final IUserRepository USER_REPOSITORY = new UserRepository(CONNECTION);

    @NotNull
    private static final String USER1_LOGIN = "test_user";

    @NotNull
    private static final String USER1_PASSWORD = "test_user";

    @NotNull
    private static final String USER1_EMAIL = "test_user@email.com";

    @NotNull
    private static String USER_ID = "";

    @BeforeClass
    public static void setUp() throws SQLException {
        @NotNull final User user = new User();
        user.setLogin(USER1_LOGIN);
        user.setPasswordHash(HashUtil.salt(PROPERTY_SERVICE, USER1_PASSWORD));
        user.setEmail(USER1_EMAIL);
        user.setRole(Role.USUAL);
        USER_REPOSITORY.add(user);
        USER_ID = user.getId();
    }

    @AfterClass
    public static void tearDown() {
        USER_REPOSITORY.removeById(USER_ID);
    }

    @Before
    public void before() throws SQLException {
        SESSION_REPOSITORY.add(USER_ID, USER1_SESSION1);
    }

    @After
    public void after() {
        SESSION_REPOSITORY.clear(USER_ID);
    }

    @Test
    public void add() throws SQLException {
        @NotNull final Session session = new Session();
        session.setDate(new Timestamp(session.getDate().getTime()));
        session.setRole(Role.USUAL);
        SESSION_REPOSITORY.add(USER_ID, session);
        @Nullable final Session createdSession = SESSION_REPOSITORY.findOneById(USER_ID, session.getId());
        Assert.assertNotNull(createdSession);
        Assert.assertEquals(session.getId(), createdSession.getId());
    }

    @Test
    public void clear() {
        Assert.assertFalse(SESSION_REPOSITORY.findAll().isEmpty());
        SESSION_REPOSITORY.clear(USER_ID);
        Assert.assertEquals(0, SESSION_REPOSITORY.findAll(USER_ID).size());
    }

    @Test
    public void findAll() {
        Assert.assertEquals(1, SESSION_REPOSITORY.findAll().size());
    }

    @Test
    public void findOneById() {
        @Nullable final Session session = SESSION_REPOSITORY.findOneById(USER_ID, USER1_SESSION1.getId());
        Assert.assertEquals(USER1_SESSION1.getId(), session.getId());
    }

    @Test
    public void existById() {
        Assert.assertTrue(SESSION_REPOSITORY.existById(USER_ID, USER1_SESSION1.getId()));
    }

    @Test
    public void remove() {
        @Nullable final Session session = SESSION_REPOSITORY.findOneById(USER_ID, USER1_SESSION1.getId());
        SESSION_REPOSITORY.remove(USER_ID, session);
        Assert.assertNull(SESSION_REPOSITORY.findOneById(USER_ID, USER1_SESSION1.getId()));
    }

    @Test
    public void removeById() {
        @Nullable final Session session = SESSION_REPOSITORY.findOneById(USER_ID, USER1_SESSION1.getId());
        SESSION_REPOSITORY.removeById(USER_ID, session.getId());
        Assert.assertNull(SESSION_REPOSITORY.findOneById(USER_ID, USER1_SESSION1.getId()));
    }

}
