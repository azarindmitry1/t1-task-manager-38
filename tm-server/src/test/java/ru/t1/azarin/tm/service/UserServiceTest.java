package ru.t1.azarin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.azarin.tm.api.service.IConnectionService;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.api.service.IUserService;
import ru.t1.azarin.tm.exception.entity.UserNotFoundException;
import ru.t1.azarin.tm.exception.field.EmailEmptyException;
import ru.t1.azarin.tm.exception.field.IdEmptyException;
import ru.t1.azarin.tm.exception.field.LoginEmptyException;
import ru.t1.azarin.tm.exception.field.PasswordEmptyException;
import ru.t1.azarin.tm.exception.user.ExistEmailException;
import ru.t1.azarin.tm.exception.user.ExistLoginException;
import ru.t1.azarin.tm.marker.UnitCategory;
import ru.t1.azarin.tm.model.User;

import java.sql.SQLException;

import static ru.t1.azarin.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IUserService service = new UserService(connectionService, propertyService);

    @NotNull
    private final String emptyString = "";

    @Nullable
    private final String nullString = null;

    @NotNull
    private final String testString = "TEST_STRING";

    @Before
    public void before() throws SQLException {
        service.add(USER1);
    }

    @After
    public void after() throws SQLException {
        if (service.findOneById(USER1.getId()) != null)
            service.remove(USER1);
    }

    @Test
    public void create() throws SQLException {
        Assert.assertThrows(LoginEmptyException.class, () ->
                service.create(emptyString, USER_UNREGISTRY_PASSWORD, USER_UNREGISTRY_EMAIL)
        );
        Assert.assertThrows(LoginEmptyException.class, () ->
                service.create(nullString, USER_UNREGISTRY_PASSWORD, USER_UNREGISTRY_EMAIL)
        );
        Assert.assertThrows(ExistLoginException.class, () ->
                service.create(USER1.getLogin(), USER1.getPasswordHash(), USER1.getEmail())
        );
        Assert.assertThrows(PasswordEmptyException.class, () ->
                service.create(USER_UNREGISTRY_LOGIN, emptyString, USER_UNREGISTRY_EMAIL)
        );
        Assert.assertThrows(PasswordEmptyException.class, () ->
                service.create(USER_UNREGISTRY_LOGIN, nullString, USER_UNREGISTRY_EMAIL)
        );
        Assert.assertThrows(ExistEmailException.class, () ->
                service.create(USER_UNREGISTRY_LOGIN, USER_UNREGISTRY_PASSWORD, USER1.getEmail())
        );
        @Nullable final User user = service.create(
                USER_UNREGISTRY_LOGIN, USER_UNREGISTRY_PASSWORD, USER_UNREGISTRY_EMAIL
        );
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_UNREGISTRY_LOGIN, user.getLogin());
        Assert.assertEquals(USER_UNREGISTRY_EMAIL, user.getEmail());
        Assert.assertEquals(service.findOneById(user.getId()).getId(), user.getId());
        service.remove(user);
    }

    @Test
    public void findByLogin() throws SQLException {
        Assert.assertThrows(LoginEmptyException.class, () ->
                service.findByLogin(emptyString)
        );
        Assert.assertThrows(LoginEmptyException.class, () ->
                service.findByLogin(nullString)
        );
        @Nullable final User user = service.findByLogin(USER1.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER1.getLogin(), user.getLogin());
    }

    @Test
    public void findByEmail() throws SQLException {
        Assert.assertThrows(EmailEmptyException.class, () ->
                service.findByEmail(emptyString)
        );
        Assert.assertThrows(EmailEmptyException.class, () ->
                service.findByEmail(nullString)
        );
        @Nullable final User user = service.findByEmail(USER1.getEmail());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER1.getEmail(), user.getEmail());
    }

    @Test
    public void isLoginExist() throws SQLException {
        Assert.assertFalse(service.isLoginExist(emptyString));
        Assert.assertFalse(service.isLoginExist(nullString));
        Assert.assertTrue(service.isLoginExist(USER1.getLogin()));
        Assert.assertFalse(service.isLoginExist(USER_UNREGISTRY_LOGIN));
    }

    @Test
    public void isEmailExist() throws SQLException {
        Assert.assertFalse(service.isEmailExist(emptyString));
        Assert.assertFalse(service.isEmailExist(nullString));
        Assert.assertTrue(service.isEmailExist(USER1.getEmail()));
        Assert.assertFalse(service.isEmailExist(USER_UNREGISTRY_EMAIL));
    }

    @Test
    public void remove() throws SQLException {
        @Nullable final User user = service.findOneById(USER1.getId());
        service.remove(user);
        Assert.assertNull(service.findOneById(USER1.getId()));
    }

    @Test
    public void removeByLogin() throws SQLException {
        Assert.assertThrows(LoginEmptyException.class, () ->
                service.removeByLogin(emptyString)
        );
        Assert.assertThrows(LoginEmptyException.class, () ->
                service.removeByLogin(nullString)
        );
        Assert.assertThrows(UserNotFoundException.class, () ->
                service.removeByLogin(USER_UNREGISTRY_LOGIN)
        );
        service.removeByLogin(USER1.getLogin());
        Assert.assertNull(service.findOneById(USER1.getId()));
    }

    @Test
    public void removeByEmail() throws SQLException {
        Assert.assertThrows(EmailEmptyException.class, () ->
                service.removeByEmail(emptyString)
        );
        Assert.assertThrows(EmailEmptyException.class, () ->
                service.removeByEmail(nullString)
        );
        Assert.assertThrows(UserNotFoundException.class, () ->
                service.removeByEmail(USER_UNREGISTRY_EMAIL)
        );
        service.removeByEmail(USER1_EMAIL);
        Assert.assertNull(service.findOneById(USER1.getId()));
    }

    @Test
    public void setPassword() throws SQLException {
        Assert.assertThrows(IdEmptyException.class, () ->
                service.setPassword(emptyString, USER_UNREGISTRY_PASSWORD)
        );
        Assert.assertThrows(IdEmptyException.class, () ->
                service.setPassword(nullString, USER_UNREGISTRY_PASSWORD)
        );
        Assert.assertThrows(PasswordEmptyException.class, () ->
                service.setPassword(USER_UNREGISTRY_ID, emptyString)
        );
        Assert.assertThrows(PasswordEmptyException.class, () ->
                service.setPassword(USER_UNREGISTRY_ID, nullString)
        );
        Assert.assertThrows(UserNotFoundException.class, () ->
                service.setPassword(USER_UNREGISTRY_ID, USER_UNREGISTRY_PASSWORD)
        );
        service.setPassword(USER1.getId(), ADMIN_PASSWORD);
        @NotNull final User user = service.findOneById(USER1.getId());
        Assert.assertEquals(ADMIN.getPasswordHash(), user.getPasswordHash());
        service.setPassword(user.getId(), USER1_PASSWORD);
    }

    @Test
    public void updateUser() throws SQLException {
        Assert.assertThrows(IdEmptyException.class, () ->
                service.updateUser(emptyString, testString, testString, testString)
        );
        Assert.assertThrows(IdEmptyException.class, () ->
                service.updateUser(nullString, testString, testString, testString)
        );
        Assert.assertThrows(UserNotFoundException.class, () ->
                service.updateUser(USER_UNREGISTRY_ID, testString, testString, testString)
        );
        @Nullable final User user = service.findOneById(USER1.getId());
        service.updateUser(USER1.getId(), testString, testString, testString);
        Assert.assertEquals(USER1.getFirstName(), user.getFirstName());
        Assert.assertEquals(USER1.getMiddleName(), user.getMiddleName());
        Assert.assertEquals(USER1.getLastName(), user.getLastName());
    }

    @Test
    public void lockUserByLogin() throws SQLException {
        Assert.assertThrows(LoginEmptyException.class, () ->
                service.lockUserByLogin(emptyString)
        );
        Assert.assertThrows(LoginEmptyException.class, () ->
                service.lockUserByLogin(nullString)
        );
        Assert.assertThrows(UserNotFoundException.class, () ->
                service.lockUserByLogin(USER_UNREGISTRY_ID)
        );
        service.lockUserByLogin(USER1.getLogin());
        @NotNull final User user = service.findOneById(USER1.getId());
        Assert.assertTrue(user.isLocked());
        service.unlockUserByLogin(USER1.getLogin());
    }

    @Test
    public void unlockUserByLogin() throws SQLException {
        Assert.assertThrows(LoginEmptyException.class, () ->
                service.unlockUserByLogin(emptyString)
        );
        Assert.assertThrows(LoginEmptyException.class, () ->
                service.unlockUserByLogin(nullString)
        );
        Assert.assertThrows(UserNotFoundException.class, () ->
                service.unlockUserByLogin(USER_UNREGISTRY_ID)
        );
        @NotNull final User user = service.findOneById(USER1.getId());
        service.unlockUserByLogin(USER1.getLogin());
        Assert.assertFalse(user.isLocked());
    }

}
