package ru.t1.azarin.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.model.Project;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectShowByIdResponse extends AbstractProjectResponse {

    public ProjectShowByIdResponse(@Nullable final Project project) {
        super(project);
    }

}
